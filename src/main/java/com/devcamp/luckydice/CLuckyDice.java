package com.devcamp.luckydice;
import java.util.Random;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
@CrossOrigin
public class CLuckyDice {
    @GetMapping("/get-dices")
    public String getDice(){
        int randomNumber = new Random().nextInt(6) + 1;
        String username = "linhlt";
        return "Xin chào " + username + ". Số may mắn hôm nay của bạn là: " + randomNumber;
    }
}
