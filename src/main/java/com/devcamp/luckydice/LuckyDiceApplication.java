package com.devcamp.luckydice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LuckyDiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(LuckyDiceApplication.class, args);
		CLuckyDice dice1 = new CLuckyDice();
		System.out.println(dice1.getDice());
	}

}
